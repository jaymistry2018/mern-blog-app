var express = require("express");
var router = express.Router();

const blogs = require("./blogs");
router.use("/blogs", blogs.route);

const users = require("./Users");
router.use("/users", users.route);

module.exports = {
  modules: {
    blogs,
  },
  router,
};
