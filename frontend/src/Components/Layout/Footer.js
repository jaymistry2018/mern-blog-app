import React from "react";

export const Footer = () => {
  return (
    <footer>
      <div
        className="container"
        style={{
          position: "absolute",
          bottom: 0,
          width: "100%",
          padding: "1rem",
          textAlign: "center",
        }}
      >
        <div className="row">
          <div className="col-md-12">
            <div className="copyright">
              Created by Jay Mistry &copy; {new Date().getFullYear()}
            </div>
            <a
              target="_blank"
              href="https://gitlab.com/jaymistry2018/mern-blog-app"
            >
              Git Lab
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};
