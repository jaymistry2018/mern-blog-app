import React, { useEffect, useState } from "react";
import { Form, Col, Button } from "react-bootstrap";
import axiosInstance from "../../services/axiosInstance";
import { Layout } from "../Layout";
import { useSelector } from "react-redux";
import { getBlogCategories, getCurrentUser } from "../../reducers/blogSlice";
import { useNavigate, useParams } from "react-router-dom";

export const AddEditBlog = () => {
  const [validated, setValidated] = useState(false);
  const [currentEditableBlog, setCurrentEditableBlog] = useState(null);
  const defaultBlogCategories = useSelector(getBlogCategories);
  const currentUser = useSelector(getCurrentUser);
  const { id } = useParams();
  const navigate = useNavigate();
  console.log(id);
  console.log(currentUser);
  // const navigate = useNavigate();
  const handleSubmit = (e) => {
    const form = e.currentTarget;
    e.preventDefault();
    e.stopPropagation();
    setValidated(true);
    if (form.checkValidity() === false) return;
    const payload = {
      title: form.title.value,
      author: currentUser?._id,
      description: form.description.value,
      category: form.category.value,
    };
    axiosInstance[currentEditableBlog ? "put" : "post"](
      `/blogs/${currentEditableBlog?._id ? currentEditableBlog?._id : ""}`,
      payload || {}
    )
      .then((response) => {
        if (response?.data?.data) {
          navigate("/");
        } else {
          window.alert(response.data.message);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    if (id) {
      axiosInstance
        .get(`/blogs/${id}`)
        .then((response) => {
          setCurrentEditableBlog(response.data.data);
        })
        .catch((error) => {
          console.log(error);
          setCurrentEditableBlog(null);
          navigate("/");
        });
    }
  }, []);

  return (
    <Layout>
      <div className="card shadow-sm col-8 border-0 px-3 rounded-2 mb-3 py-4 mx-auto mt-5 bg-light">
        <div className="card-header bg-transparent border-0 text-center text-uppercase">
          <h3>{currentEditableBlog ? "Edit Blog" : "Add Blog"}</h3>
        </div>
        <div className="card-body">
          <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Form.Group as={Col} controlId="validationCustom01">
              <Form.Label>Title</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Enter blog title"
                name="title"
                defaultValue={currentEditableBlog?.title || ""}
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                Title is required
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} controlId="validationCustom02">
              <Form.Label className="mt-2">Description</Form.Label>
              <Form.Control
                required
                as="textarea"
                rows="8"
                placeholder="Description"
                name="description"
                defaultValue={currentEditableBlog?.description || ""}
              />
              <Form.Control.Feedback type="invalid">
                Description is required
              </Form.Control.Feedback>
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} controlId="formGridState">
              <Form.Label className="mt-2">Category</Form.Label>
              <Form.Select
                name="category"
                defaultValue={
                  currentEditableBlog?.category || defaultBlogCategories[0]
                }
              >
                {defaultBlogCategories.map((category) => (
                  <option key={category} value={category}>
                    {category}
                  </option>
                ))}
              </Form.Select>
            </Form.Group>
            <Button className="col-12 my-4" variant="primary" type="submit">
              {currentEditableBlog ? "Update" : "Add"}
            </Button>
          </Form>
        </div>
      </div>
    </Layout>
  );
};
