import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setCurrentUser } from "../../reducers/blogSlice";

const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const paths = [
    {
      path: "/",
      text: "Home",
    },
    {
      path: "/create-blog",
      text: "Add a new blog",
    },
    {
      path: "/profile",
      text: "Profile",
    },
  ];

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#">MERN Blog App</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            {paths.map((path, index) => {
              return (
                <Nav.Link
                  key={index}
                  onClick={() => {
                    navigate(path.path);
                  }}
                  className="nav-link"
                  style={{
                    fontWeight:
                      window.location.pathname == path.path ? "bold" : "italic",
                  }}
                >
                  {path.text}
                </Nav.Link>
              );
            })}
            <Nav.Link
              onClick={() => {
                dispatch(setCurrentUser(null));
              }}
            >
              Logout
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
