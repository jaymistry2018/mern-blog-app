import { Login } from "../Components/Auth/Login";

import { Navigate } from "react-router-dom";
import { SignUp } from "../Components/Auth/SignUp";

import { Blogs } from "../Components/Blogs/";
import { AddEditBlog } from "../Components/Blogs/AddEditBlog";

const publicRoutes = [
  {
    path: "/auth/login",
    component: <Login />,
  },
  {
    path: "/auth/sign-up",
    component: <SignUp />,
  },
  {
    path: "*",
    component: <Navigate to="/auth/login" />,
  },
];
const protectedRoutes = [
  {
    path: "/",
    component: <Blogs />,
  },
  {
    path: "/create-blog",
    component: <AddEditBlog />,
  },
  {
    path: "/edit-blog/:id",
    component: <AddEditBlog />,
  },
  {
    path: "/blogs/:id",
    // component: <Post />,
  },
  {
    path: "*",
    component: <Navigate to="/" />,
  },
];

export { publicRoutes, protectedRoutes };
