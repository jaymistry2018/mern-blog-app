import { configureStore } from "@reduxjs/toolkit";
import { blogSlice } from "./reducers/blogSlice";

export const store = configureStore({
  reducer: {
    blogs: blogSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
