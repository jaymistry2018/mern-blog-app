import React, { useEffect } from "react";
import { Layout } from "../Layout";
import { useDispatch } from "react-redux";
import { getAllBlogsByAPI } from "../../reducers/blogSlice";
import { BlogList } from "./BlogList";

export const Blogs = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllBlogsByAPI());
  }, []);

  return (
    <Layout>
      <h1
        style={{
          textAlign: "center",
          fontSize: "3rem",
          fontWeight: "bold",
          color: "black",
          marginTop: "2rem",
          marginBottom: "2rem",
        }}
      >
        Blogs
      </h1>
      <BlogList />
    </Layout>
  );
};
