const express = require("express");
const routes = express.Router();
const controller = require("./controller");

routes.get("/", controller.getAllBlogs);
routes.get("/:id", controller.getBlogById);
routes.post("/", controller.createBlog);
routes.put("/:id", controller.updateBlog);
routes.delete("/:id", controller.deleteBlog);
module.exports = routes;
