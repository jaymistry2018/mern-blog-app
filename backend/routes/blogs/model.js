const mongoose = require("mongoose");
const blogSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
    description: {
      type: String,
      required: true,
    },
    category: {
      type: String,
      required: true,
      enum: ["Food", "Educations", "Businessmen", "Positions"],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("blogs", blogSchema);
