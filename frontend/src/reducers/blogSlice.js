import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../services/axiosInstance";

const getLoggedInUser = () => {
  if (localStorage.getItem("currentUser")) {
    return JSON.parse(localStorage.getItem("currentUser"));
  }
  return null;
};

const initialState = {
  currentUser: getLoggedInUser(),
  blogs: [],
  blogCategories: ["Food", "Educations", "Businessmen", "Positions"],
};

export const getAllBlogsByAPI = createAsyncThunk(
  "blogs/getAllBlogs",
  async () => {
    const response = await axiosInstance.get("/blogs");
    return response.data;
  }
);

export const blogSlice = createSlice({
  name: "blogs",
  initialState,
  reducers: {
    setBlogs: (state, action) => {
      state.blogs = action.payload;
    },
    setBlogCategories: (state, action) => {
      state.blogCategories = action.payload;
    },
    setCurrentUser: (state, action) => {
      state.currentUser = action.payload;
      localStorage.setItem("currentUser", JSON.stringify(state.currentUser));
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getAllBlogsByAPI.fulfilled, (state, action) => {
      state.blogs = action.payload?.data;
    });
  },
});

export const {
  setBlogs,
  setBlogCategories,
  setCurrentUser,
  setCurrentEditableBlog,
} = blogSlice.actions;

export const getBlogs = (state) => state.blogs.blogs;
export const getBlogCategories = (state) => state.blogs.blogCategories;
export const getCurrentUser = (state) => state.blogs.currentUser;
export const getAllBlogs = (state) => state.blogs.blogs;

export default blogSlice.reducer;
