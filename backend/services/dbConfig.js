//for db connection and configuration
exports.dbConfig = {
  host: "mongodb://127.0.0.1:27017/blog_app",
  url: "mongodb://127.0.0.1:27017/blog_app",
  dbName: "blog_app",
  port: 27017,
};
