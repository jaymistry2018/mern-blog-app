const model = require("./model");

exports.getAllBlogs = async (req, res) => {
  try {
    const resObj = {};
    await model
      .find()
      .populate({
        path: "author",
        select: "email",
      })
      .lean(true)
      .then((data) => {
        resObj.status = 200;
        resObj.message = "Success";
        resObj.data = data;
      });

    res.status(200).json(resObj);
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
};

exports.getBlogById = async (req, res) => {
  try {
    const resObj = {};
    await model
      .findById(req.params.id)
      .populate({
        path: "author",
        select: "name",
      })
      .lean(true)
      .then((data) => {
        resObj.status = 200;
        resObj.message = "Success";
        resObj.data = data;
      });

    res.status(200).json(resObj);
  } catch (err) {
    res.status(500).json(err);
  }
};

exports.createBlog = async (req, res) => {
  try {
    const resObj = {};
    await model.create(req.body).then((data) => {
      resObj.status = 200;
      resObj.message = "Success";
      resObj.data = data;
    });

    res.status(200).json(resObj);
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
};

exports.updateBlog = async (req, res) => {
  try {
    const resObj = {};
    await model.findByIdAndUpdate(req.params.id, req.body).then((data) => {
      resObj.status = 200;
      resObj.message = "Success";
      resObj.data = data;
    });

    res.status(200).json(resObj);
  } catch (err) {
    res.status(500).json(err);
  }
};

exports.deleteBlog = async (req, res) => {
  try {
    const resObj = {};
    await model
      .findByIdAndDelete(req.params.id)
      .lean(true)
      .then((data) => {
        resObj.status = 200;
        resObj.message = "Success";
        resObj.data = data;
      });

    res.status(200).json(resObj);
  } catch (err) {
    res.status(500).json(err);
  }
};
