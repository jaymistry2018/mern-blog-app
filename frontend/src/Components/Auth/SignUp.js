import React, { useState } from "react";
import { Form, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import axiosInstance from "../../services/axiosInstance";
import { useDispatch } from "react-redux";
import { setCurrentUser } from "../../reducers/blogSlice";

export const SignUp = () => {
  const [validated, setValidated] = useState(false);
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    const form = e.currentTarget;
    e.preventDefault();
    setValidated(true);
    if (form.checkValidity() === false) return;
    const payload = {
      firstName: e.target.firstName.value,
      lastName: e.target.lastName.value,
      email: e.target.email.value,
      password: e.target.password.value,
    };
    axiosInstance
      .post("/users/new", payload)
      .then((response) => {
        if (response?.data?.data) {
          dispatch(setCurrentUser(response.data.data));
        } else {
          window.alert(response.data.message);
        }
      })
      .catch((err) => console.error(err));
  };
  return (
    <div className="card shadow-sm col-4 border-0 px-3 rounded-2 mb-3 py-4 mx-auto mt-5 bg-light">
      <div className="card-header bg-transparent border-0 text-center text-uppercase">
        <h3>Sign Up</h3>
      </div>
      <div className="card-body">
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
          <Form.Group as={Col} controlId="validationCustom01">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              required
              type="text"
              placeholder="First Name"
              name="firstName"
            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            <Form.Control.Feedback type="invalid">
              First Name is required
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} controlId="validationCustom02">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              required
              type="text"
              placeholder="Last Name"
              name="lastName"
            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            <Form.Control.Feedback type="invalid">
              Last Name is required
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} controlId="validationCustom03">
            <Form.Label>Email</Form.Label>
            <Form.Control
              required
              type="text"
              placeholder="Email"
              name="email"
            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            <Form.Control.Feedback type="invalid">
              Email is required
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} controlId="validationCustom04">
            <Form.Label>Password</Form.Label>
            <Form.Control
              required
              type="password"
              placeholder="Password"
              name="password"
            />
            <Form.Control.Feedback type="invalid">
              Password is required
            </Form.Control.Feedback>
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
          </Form.Group>
          <Button className="col-12 my-4" variant="primary" type="submit">
            Sign Up
          </Button>
        </Form>
        <div className="text-center  mt-3">
          <Form.Text>
            Already have an account? <Link to="/auth/login">Login</Link>
          </Form.Text>
        </div>
      </div>
    </div>
  );
};
