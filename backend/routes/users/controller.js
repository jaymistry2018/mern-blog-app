const model = require("./model");

exports.loginUser = async (req, res) => {
  try {
    const resObj = {};
    await model
      .findOne({ email: req.body.email })
      .then((result) => {
        if (result) {
          if (result.password === req.body.password) {
            resObj.status = 200;
            resObj.message = "Success";
            resObj.data = result;
          } else {
            resObj.status = 401;
            resObj.message = "Invalid Password";
          }
          res.status(200).json(resObj);
        } else {
          resObj.status = 200;
          resObj.message = "User not found";
          resObj.data = result;
          res.status(200).json(resObj);
          return;
        }
      })
      .catch((err) => {
        console.log(err);
        resObj.status = 500;
        resObj.message = "Internal Server Error";
        resObj.data = err;
        res.status(500).json(resObj);
        return;
      });
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
};

exports.createUser = async (req, res) => {
  try {
    const resObj = {};
    await model.findOne({ email: req.body.email }).then(async (user) => {
      if (user) {
        resObj.status = 500;
        resObj.message = "User already exists";
        res.status(200).json(resObj);
        return;
      } else {
        await model
          .create(req.body)
          .then((response) => {
            resObj.status = 200;
            resObj.message = "Success";
            resObj.data = response;
            res.status(200).json(resObj);
            return;
          })
          .catch((error) => {
            resObj.status = 500;
            resObj.message = "Internal Server Error";
            resObj.data = error;
            res.status(500).json(resObj);
            return;
          });
        return;
      }
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      message: "Internal Server Error",
      data: err,
    });
  }
};

exports.getAllUsers = async (req, res) => {
  try {
    const resObj = {};
    await model
      .find()
      .lean(true)
      .then((data) => {
        resObj.status = 200;
        resObj.message = "Success";
        resObj.data = data;
      });

    res.status(200).json(resObj);
  } catch (err) {
    res.status(500).json(err);
  }
};
