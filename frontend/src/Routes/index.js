import React from "react";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import { protectedRoutes, publicRoutes } from "./config";
import { useSelector } from "react-redux";
import { getCurrentUser } from "../reducers/blogSlice";

export const AppRoutes = () => {
  const loggedInUser = useSelector(getCurrentUser);
  const routes = loggedInUser ? protectedRoutes : publicRoutes;
  return (
    <Router>
      <Routes>
        {routes?.map((route) => (
          <Route key={route.path} path={route.path} element={route.component} />
        ))}
      </Routes>
    </Router>
  );
};
