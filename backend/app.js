var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const mongoose = require("mongoose");
const { dbConfig } = require("./services/dbConfig");

var indexRouter = require("./routes/index").router;
var app = express();
var cors = require("cors");

app.use(cors());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.get("/", function (req, res, next) {
  res.render("index", { title: "Blog App backend" });
});

app.use("/api", indexRouter);

// connect to mongo db
const mongoUri = dbConfig.host;
mongoose.Promise = global.Promise;
mongoose.connect(mongoUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  keepAlive: true,
});
mongoose.connection.on("connected", () => {
  console.log(`connected to database: ${dbConfig.dbName}`);
});
mongoose.connection.on("error", (err) => {
  console.log(err);
  throw new Error(`unable to connect to database: ${dbConfig.dbName}`);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
