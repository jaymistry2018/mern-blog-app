import React, { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllBlogsByAPI,
  getBlogs,
  getCurrentUser,
} from "../../reducers/blogSlice";
import { useNavigate } from "react-router-dom";
import axiosInstance from "../../services/axiosInstance";

export const BlogList = () => {
  const allBlogs = useSelector(getBlogs);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [search, setSearch] = useState("");
  const [blogs, setBlogs] = useState([]);
  const [isAsscending, setIsAsscending] = useState(true);

  const currentUser = useSelector(getCurrentUser);

  const onDelete = (_id) => {
    axiosInstance.delete(`blogs/${_id}`).then((response) => {
      if (response.status === 200) {
        dispatch(getAllBlogsByAPI());
      }
    });
  };

  const BlogTemplate = ({ blog }) => (
    <div className="card shadow-sm col-auto border-0 px-3 rounded-2 mb-3 py-4 mx-auto mt-5 bg-light">
      <div className="card-header bg-transparent border-0 text-center text-uppercase">
        <h3>{blog?.title}</h3>
      </div>
      <div className="card-body">
        <label className="col-md">Description</label>
        <p className="card-text" name="description">
          {blog?.description}
        </p>
        <hr />
        <label className="col-md">Author - {blog?.author?.email}</label>
      </div>
      <div className="card-footer text-center text-uppercase">
        <label>{new Date(blog?.createdAt).toDateString()}</label>
        <Button
          disabled={blog?.author?._id !== currentUser?._id}
          size="sm"
          className="mx-2"
          onClick={() => {
            navigate(`/edit-blog/${blog._id}`);
          }}
        >
          Edit
        </Button>
        <Button
          disabled={blog?.author?._id !== currentUser?._id}
          variant="danger"
          size="sm"
          onClick={() => onDelete(blog?._id)}
        >
          Delete
        </Button>
      </div>
    </div>
  );

  const handleSearch = (e) => {
    const term = e.target.value;
    setSearch(term);
    const filteredBlogs = allBlogs?.filter((blog) => {
      return (
        blog?.title.toLowerCase().includes(term.toLowerCase()) ||
        blog?.description.toLowerCase().includes(term.toLowerCase()) ||
        blog?.author?.email.toLowerCase().includes(term.toLowerCase())
      );
    });
    setBlogs(filteredBlogs);
  };

  const handleSort = () => {
    setIsAsscending(!isAsscending);
    setBlogs(allBlogs.slice().sort(() => (!isAsscending ? -1 : 1)));
  };

  useEffect(() => {
    setBlogs(allBlogs);
  }, [allBlogs]);

  return (
    <div
      style={{
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-between",
        margin: "5%",
      }}
    >
      <div className="col-9">
        <Form.Control
          type="text"
          placeholder="Search blogs by author or title"
          onChange={handleSearch}
          value={search}
          className="form-control"
        />
      </div>
      <div className="col-auto">
        <Button onClick={handleSort} variant="primary">
          Sort by Date ({isAsscending ? "asc" : "desc"})
        </Button>
      </div>
      {blogs?.length ? (
        blogs?.map((blog) => <BlogTemplate key={blog?._id} blog={blog} />)
      ) : (
        <div
          style={{
            height: "50vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
          }}
          className="text-center col-12"
        >
          <h3>No blogs found</h3>
        </div>
      )}
    </div>
  );
};
