const express = require("express");
const routes = express.Router();
const controller = require("./controller");

routes.post("/new", controller.createUser);
routes.post("/login", controller.loginUser);
routes.get("/", controller.getAllUsers);
module.exports = routes;
